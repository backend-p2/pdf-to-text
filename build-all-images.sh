#!/bin/bash

# build backend images
service_paths=`ls -d backend/*/` # ls only folders
for path in $service_paths
do
    folder=${path#*/} # remove backend/ at the start
    folder=${folder%/} # remove / at the end

    docker build -t bright1h/$folder $path # use p2/$folder as image name
    docker push bright1h/$folder 
    # docker save -o docker-images/$folder.tar p2/$folder
done

# build frontend image
docker build -t bright1h/frontend frontend
# docker save -o docker-images/frontend.tar frontend
docker push bright1h/frontend

docker build -t bright1h/nginx-proxy nginx-proxy
# docker save -o docker-images/nginx-proxy.tar p2/nginx-proxy
docker push bright1h/nginx-proxy
