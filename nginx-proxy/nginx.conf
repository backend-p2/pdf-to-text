worker_processes 1;

events { worker_connections 1024; }

error_log /dev/stdout info;

http {
    # client_max_body_size 200M;
    proxy_buffering off;

    upstream web-controller {
        server web-controller:5000;
    }

    upstream frontend {
        server frontend:80;
    }

    server {
        listen 8000;

        client_max_body_size 0;

        location /api {
            proxy_pass http://web-controller;
            
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;

            # Fixing 504 Gateway Timeout
            proxy_connect_timeout 600;
            proxy_send_timeout    600;
            proxy_read_timeout    600;
            send_timeout          600;
        }

        location /socket.io {
            proxy_pass http://web-controller;

            proxy_redirect      off;
            proxy_set_header    Host $host;
            proxy_set_header    X-Real-IP $remote_addr;
            proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header    X-Forwarded-Host $server_name;

            proxy_http_version  1.1;
            proxy_set_header    Upgrade $http_upgrade;
            proxy_set_header    Connection "upgrade";
        }

        location / {
            proxy_pass http://frontend;
                        
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;

            # Fixing 504 Gateway Timeout
            proxy_connect_timeout 600;
            proxy_send_timeout    600;
            proxy_read_timeout    600;
            send_timeout          600;
        }
    }
}