#!/bin/bash


config_paths=`ls -d k8s/*/` # ls only folders
for path in $config_paths
do
    kubectl delete -f $path
    kubectl create -f $path
done